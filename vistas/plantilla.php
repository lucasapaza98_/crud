<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
	<script src="https://kit.fontawesome.com/c5b742650e.js" crossorigin="anonymous"></script> 
	<title>Ejercicio MVC</title>
</head>
<body>
	<div class="container-fluid bg-light">
		<h3 class="text-center py-3">LOGO</h3>
	</div>
	<div class="container">
				<ul class="nav nav-justified py-2 nav-pills">
					<?php /* Las variables GET se pasan con parámetros URL. También conocido como cadena
							 de consultas a través de URL cuando es la primera variable se separa con
							 signo de pregunta Las que siguen a continuación se separan con &.*/ ?>
					<?php /*	*** NAVEGACION CON VARIABLE GET ***		*/?>
					<?php if (isset($_GET['pagina'])): ?>
						
						<?php if ($_GET['pagina'] == "registro"): ?>
							<li class="nav-item">
								<a class="nav-link active" href="index.php?pagina=registro">Registro</a>
							</li>
						<?php else: ?>
							<li class="nav-item">
								<a class="nav-link" href="index.php?pagina=registro">Registro</a>
							</li>		
						<?php endif ?>
						<?php if ($_GET['pagina'] == "ingreso"): ?>
							<li class="nav-item">
								<a class="nav-link active" href="index.php?pagina=ingreso">Ingreso</a>
							</li>
						<?php else: ?>
							<li class="nav-item">
								<a class="nav-link" href="index.php?pagina=ingreso">Ingreso</a>
							</li>		
						<?php endif ?>
						<?php if ($_GET['pagina'] == "inicio"): ?>
							<li class="nav-item">
								<a class="nav-link active" href="index.php?pagina=inicio">Inicio</a>
							</li>
						<?php else: ?>
							<li class="nav-item">
								<a class="nav-link" href="index.php?pagina=inicio">Inicio</a>
							</li>		
						<?php endif ?>
						<?php if ($_GET['pagina'] == "salir"): ?>
							<li class="nav-item">
								<a class="nav-link active" href="index.php?pagina=salir">Salir</a>
							</li>
						<?php else: ?>
							<li class="nav-item">
								<a class="nav-link" href="index.php?pagina=salir">Salir</a>
							</li>		
						<?php endif ?>
						<?php else: ?>
							<li class="nav-item">
						<a class="nav-link" href="index.php?pagina=registro">Registro</a>
					</li>	
					<li class="nav-item">
						<a class="nav-link" href="index.php?pagina=ingreso">Ingreso</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="index.php?pagina=inicio">Inicio</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="index.php?pagina=salir">Salir</a>
					</li>
					<?php endif ?>					
				</ul>				
	</div>
	<div class="container-fluid bg-light">
		<div class="container py-5">
			<?php
				if(isset($_GET['pagina'])) {
					if( ($_GET['pagina'] == "registro") || ($_GET['pagina'] == "ingreso") || ($_GET['pagina'] == "inicio") || ($_GET['pagina'] == "salir") || ($_GET['pagina'] == "editar") ) {

						include "paginas/".$_GET['pagina'].".php";		
					}else{
						include "paginas/error404.php";		
					}

				}else{
					include "paginas/registro.php";	
				}				
			?>
		</div>
	</div>
</body>
</html>