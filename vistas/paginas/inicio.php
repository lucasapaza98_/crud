<?php 
if(isset($_SESSION['validarIngreso'])) {
	if($_SESSION['validarIngreso'] != "ok"){
		echo '<script>window.location = "index.php?pagina=ingreso";</script>';
		return;
	}
}else{
	echo '<script>window.location = "index.php?pagina=ingreso";</script>';
	return;
}
$usuarios = ControladorFormularios::ctrSelecRegistros(null, null);
//echo '<pre>'; print_r($usuarios); echo '</pre>';
// actualizar como no estatico

?>

<table class="table table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Nombre Completo</th>
			<th>Email</th>
			<th>Fecha</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($usuarios as $key => $value): ?>
			<tr>
				<td><?php echo ($key+1) ?> </td>
				<td><?php echo $value['nombreUsuario'] ?></td>
				<td><?php echo $value['emailUsuario'] ?></td>
				<td><?php echo $value['fecha'] ?></td>
				<td>
					<div class="btn-group">
						<div class="px-1">
							<a href="index.php?pagina=editar&id=<?php echo $value['idUsuario']; ?>" class="btn btn-warning"><i class="fas fa-edit"> </i></a>	
						</div>
						<form method="post">							
							<input type="hidden" value="<?php echo $value['idUsuario']; ?>" name="delRegistro" >
							<button type="submit" class="btn btn-danger"> <i class="fas fa-trash-alt"></i></button>	
							<?php 
							// con metodo no estatico
								$eliminar = new ControladorFormularios();
								$eliminar -> ctrEliminarRegistro();
							?>
						</form>						
					</div>
				</td>
			</tr>
		<?php endforeach ?>		
	</tbody>
</table>