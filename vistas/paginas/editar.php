<?php 
if(isset($_GET['id'])){
	$item = "idUsuario";
	$valor = $_GET['id'];
	//var_dump($valor);
	$usuario = ControladorFormularios::ctrSelecRegistros($item, $valor);

	//echo '<pre>'; print_r($usuario); echo '</pre>';
}
?>
<div class="d-flex justify-content-center text-center">
<form class="p-5 bg-light" method="post">
	<div class="form-group">	
		<div class="input-group">
			<div class="input-group-prepend">
				<span class="input-group-text">
					<i class="fas fa-user"></i>
				</span>
				<input type="text" name="nameUpdt" class="form-control" value="<?php echo $usuario['nombreUsuario'] ?>" placeholder="Actualizar Nombre" id="nameUpdt">		
			</div>	
		</div>
	</div>
	<div class="form-group">	
		<div class="input-group">
			<div class="input-group-prepend">
				<span class="input-group-text">
					<i class="fas fa-envelope"></i>
				</span>
				<input type="email" name="emailUpdt" class="form-control" value="<?php echo $usuario['emailUsuario'] ?>" placeholder="Actualizar Correo" id="emailUpdt">		
			</div>	
		</div>
	</div>
	<div class="form-group">	
		<div class="input-group">
			<div class="input-group-prepend">
				<span class="input-group-text">
					<i class="fas fa-key"></i>
				</span>
				<input type="password" name="pwdUpdt" class="form-control" placeholder="Actualizar Clave" id="pwdUpdt">
				<input type="hidden" name="pwdActual" value="<?php echo $usuario['passUsuario'] ?>" >
				<input type="hidden" name="idUsuario" value="<?php echo $usuario['idUsuario'] ?>" >
			</div>	
		</div>
	</div>
	<?php 
		// HAGO LA SOLICITUD AL CONTROLADOR, EL CONTROLADOR EJECUTA EL MODELO Y EL MODELO LE DA LA RESPESTA AL CONTROLADOR,
		// LUEGO EL CONTROLADOR DEVUELVE LA RESPUESTA A LA VISTA Y LA VISTA DECIDE QUE HACE CON ESA INFORMACION
		$actualizar = ControladorFormularios::ctrActualizarRegistro();
		
		if($actualizar) {
				echo '<script>
				if ( window.history.replaceState ) {
					window.history.replaceState(null, null, window.location.href);
				}
				</script>';// este script borra las variables post por si se actualiza la pagina

			echo'<div class="alert alert-success">Se actualizaron los datos</div>';//Muestra confirmacion
			echo '<script>
				setTimeout(function() {
					window.location = "index.php?pagina=inicio";
				},3000);
				</script>';//REDIRECCION 3 SEGUNDOS DESPUES
			}
	?>
	<button type="submit" class="btn btn-primary">Actualizar</button>
</form>
</div>