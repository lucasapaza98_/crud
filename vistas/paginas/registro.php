<div class="d-flex justify-content-center text-center">
<form class="p-5 bg-light" method="post">
	<div class="form-group">
		<label for="nameJoin">Nombre completo:</label>
		<div class="input-group">
			<div class="input-group-prepend">
				<span class="input-group-text">
					<i class="fas fa-user"></i>
				</span>
				<input type="text" name="nameJoin" class="form-control" placeholder="Juan Perez" id="nameJoin">		
			</div>	
		</div>
	</div>
	<div class="form-group">
		<label for="emailJoin">Email:</label>
		<div class="input-group">
			<div class="input-group-prepend">
				<span class="input-group-text">
					<i class="fas fa-envelope"></i>
				</span>
				<input type="email" name="emailJoin" class="form-control" placeholder="juan@perez" id="emailJoin">		
			</div>	
		</div>
	</div>
	<div class="form-group">
		<label for="pwdJoin">Password:</label>
		<div class="input-group">
			<div class="input-group-prepend">
				<span class="input-group-text">
					<i class="fas fa-key"></i>
				</span>
				<input type="password" name="pwdJoin" class="form-control" placeholder="clave" id="pwdJoin">		
			</div>	
		</div>
	</div>
	<?php 
	/*
	INSTANCIAR UN METODO NO ESTATICO
	*/

	//$registro = new ControladorFormularios();
	//$registro -> ctrRegistro();
	
	// METODOS ESTATICCOS
	$registro = ControladorFormularios::ctrRegistro();
	//var_dump($registro);
	if($registro) {
		echo '<script>
			if ( window.history.replaceState ) {
				window.history.replaceState(null, null, window.location.href);
			}
			</script>';// este script borra las variables post por si se actualiza la pagina

		echo'<div class="alert alert-success">El usuario ha sido registrado</div>';
	}
?>
	<button type="submit" class="btn btn-primary">Enviar</button>
</form>
</div>