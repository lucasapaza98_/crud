<div class="d-flex justify-content-center text-center">
<form class="p-5 bg-light" method="post">
	<div class="form-group">
		<label for="emailSign">Email:</label>
		<div class="input-group">
			<div class="input-group-prepend">
				<span class="input-group-text">
					<i class="fas fa-envelope"></i>
				</span>
				<input type="email" name="emailSign" class="form-control" placeholder="juan@perez" id="emailSign">		
			</div>	
		</div>
	</div>
	<div class="form-group">
		<label for="pwdSign">Password:</label>
		<div class="input-group">
			<div class="input-group-prepend">
				<span class="input-group-text">
					<i class="fas fa-key"></i>
				</span>
				<input type="password" name="pwdSign" class="form-control" placeholder="clave" id="pwdSign">		
			</div>	
		</div>
	</div>
	<?php 
		$ingreso = new ControladorFormularios();
		$ingreso -> ctrIngreso();
	?>
	<button type="submit" class="btn btn-primary">Ingresar</button>
</form>
</div>