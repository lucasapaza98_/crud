<?php  
require_once "conexion.php";
class ModeloFormularios {
	//Registro - mdl = modelo
	static public function mdlRegistro($tabla, $datos){
		// Statement = Declaracion
		// : se agrega al principio para que sean parametros ocultos		
		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(nombreUsuario, emailUsuario, passUsuario) VALUES (:nombreUsuario, :emailUsuario, :passUsuario)");;
		$stmt->bindParam(":nombreUsuario", $datos["nombreUsuario"], PDO::PARAM_STR);
		$stmt->bindParam(":emailUsuario", $datos["emailUsuario"], PDO::PARAM_STR);
		$stmt->bindParam(":passUsuario", $datos["passUsuario"], PDO::PARAM_STR);
		// Si se esta ejecutando la sentencia SQL
		if($stmt->execute()) {
			// si se ejecuta retorno el OK		
			return true;
		} else { // sino imprimo el error
			print_r(Conexion::conectar() -> errorInfo());
		}		
		$stmt -> close();//cierro la conexion
		$stmt = null; // por seguridad vaciamos el objeto de la conexion				
	}
	// consultar a la DB
	static public function mdlSelecRegistros($tabla, $item, $valor){
		if( ($item == null) && ($valor == null) ){
			$stmt = Conexion::conectar()->prepare("SELECT *,DATE_FORMAT(fecha, '%d/%m/%Y') AS fecha FROM $tabla");
			$stmt->execute();
			return $stmt->fetchAll(); // devuelve todo	
		} else {			
			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");
			$stmt->bindParam(":".$item, $valor, PDO::PARAM_STR);
			$stmt->execute();
			return $stmt->fetch(); // devuelve todo	
		}
		
		$stmt -> close();//cierro la conexion
		$stmt = null; // por seguridad vaciamos el objeto de la conexion				
	}
	static public function mdlActualizarRegistro($tabla, $datos){
		// Statement = Declaracion
		// : se agrega al principio para que sean parametros ocultos		
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombreUsuario = :nombreUsuario, emailUsuario = :emailUsuario, passUsuario = :passUsuario WHERE idUsuario = :idUsuario");
		$stmt->bindParam(":idUsuario", $datos["idUsuario"], PDO::PARAM_INT);
		$stmt->bindParam(":nombreUsuario", $datos["nombreUsuario"], PDO::PARAM_STR);
		$stmt->bindParam(":emailUsuario", $datos["emailUsuario"], PDO::PARAM_STR);
		$stmt->bindParam(":passUsuario", $datos["passUsuario"], PDO::PARAM_STR);
		// Si se esta ejecutando la sentencia SQL
		if($stmt->execute()) {
			// si se ejecuta retorno el OK		
			return true;
		} else { // sino imprimo el error
			print_r(Conexion::conectar() -> errorInfo());
		}		
		$stmt -> close();//cierro la conexion
		$stmt = null; // por seguridad vaciamos el objeto de la conexion				
	}
	static public function mdlEliminarRegistro($tabla, $valor){
		// Statement = Declaracion
		// : se agrega al principio para que sean parametros ocultos		
		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE idUsuario = :idUsuario");
		$stmt->bindParam(":idUsuario", $valor, PDO::PARAM_INT);		
		// Si se esta ejecutando la sentencia SQL
		if($stmt->execute()) {
			// si se ejecuta retorno el OK		
			return true;
		} else { // sino imprimo el error
			print_r(Conexion::conectar() -> errorInfo());
		}		
		$stmt -> close();//cierro la conexion
		$stmt = null; // por seguridad vaciamos el objeto de la conexion				
	}
}
?>