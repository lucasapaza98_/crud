<?php
class ControladorFormularios {
	// Registrar usuario
	static public function ctrRegistro() {
		if(isset($_POST['nameJoin'])){
			$tabla = "usuarios";
			$datos = array( "nombreUsuario" => $_POST['nameJoin'],
							"emailUsuario" => $_POST['emailJoin'],
							"passUsuario" => $_POST['pwdJoin'] );
			//var_dump($datos);
			$respuesta = ModeloFormularios::mdlRegistro($tabla, $datos);
			//echo "vardump_respuesta";
			//var_dump($respuesta);
			return $respuesta;
		}
	}
	// Traer regirtros desde la DB
	static public function ctrSelecRegistros($item, $valor){
		/* enviar al modelo y pedir una respuesta */
		$tabla = "usuarios";
		$respuesta = ModeloFormularios::mdlSelecRegistros($tabla, $item, $valor);
		// enviar la respuesta a la vista
		return $respuesta;
	}
	// Ingreso sera metodo no estatico
	public function ctrIngreso() {
		if(isset($_POST['emailSign'])) {
			$tabla = "usuarios";
			$item = "emailUsuario";
			$valor = $_POST['emailSign'];
			$respuesta = ModeloFormularios::mdlSelecRegistros($tabla, $item, $valor);
			if( ($respuesta['emailUsuario'] == $_POST['emailSign']) && ($respuesta['passUsuario'] == $_POST['pwdSign']) ) {
				$_SESSION['validarIngreso'] = "ok";
				echo '<script>
					if ( window.history.replaceState ) {
						window.history.replaceState(null, null, window.location.href);
					}
					window.location = "index.php?pagina=inicio";
					</script>';
			} else {
				echo '<script>
					if ( window.history.replaceState ) {
						window.history.replaceState(null, null, window.location.href);
					}
					</script>';
				echo'<div class="alert alert-danger">Usuario o Contraseña incorrecta</div>';
			}
			//echo '<pre>'; print_r($respuesta); echo '</pre>';
		}
	}
	//Actualizar registro, no estatico
	static public function ctrActualizarRegistro(){
		if(isset($_POST['nameUpdt'])){
			if($_POST['pwdUpdt'] != ""){
				$pass = $_POST['pwdUpdt'];
			} else {
				$pass = $_POST['pwdActual'];
			}
			$tabla = "usuarios";
			$datos = array( "idUsuario" => $_POST['idUsuario'],
							"nombreUsuario" => $_POST['nameUpdt'],
							"emailUsuario" => $_POST['emailUpdt'],
							"passUsuario" => $pass );
			//var_dump($datos);
			$respuesta = ModeloFormularios::mdlActualizarRegistro($tabla, $datos);

			return $respuesta;
			//echo "vardump_respuesta";
			//var_dump($respuesta);			
		}
	}
	public function ctrEliminarRegistro() {
		if(isset($_POST['delRegistro'])) {
			$tabla = "usuarios";
			$valor = $_POST['delRegistro'];
			//var_dump($valor);
			$respuesta = ModeloFormularios::mdlEliminarRegistro($tabla, $valor);
			return $respuesta;
			if($respuesta) {
				// esto elimina variables cargadas en la pagina para que no se reenvie informacion
				echo '<script>
					if ( window.history.replaceState ) {
						window.history.replaceState(null, null, window.location.href);
					}
					window.location = "index.php?pagina=inicio";
					</script>';// y luego redirecciona a inicio nuevamente
			}
		}
	}
}
?>